# Agregar Badge de última versión de Joomla

Agrega un Badge de la versión actual de Joomla

Si deseas agregar un Badge con la versión actual de Joomla simplemente agrega la siguiente url como una imagen:  
https://img.shields.io/badge/dynamic/json.svg?label=Joomla&url=https%3A%2F%2Fwww.javimata.com%2Flab%2Fjoomla%2Fversion.php&query=version&colorA=black&colorB=5091CD&logo=joomla

Esto lo que hace es, utilizando el servicio dinamico de Shields para la creación de badges y un pequeño script en mi sitio que regresa la última versión de Joomla crea el Badge de manera dinamica.

ej: <a href="https://www.joomla.org" target="_blank"><img src="https://img.shields.io/badge/dynamic/json.svg?label=Joomla&url=https%3A%2F%2Fwww.javimata.com%2Flab%2Fjoomla%2Fversion.php&query=version&colorA=black&colorB=5091CD&logo=joomla"></a>